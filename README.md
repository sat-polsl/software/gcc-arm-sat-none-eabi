# gcc-arm-sat-eabi

## Content
* **gcc-arm-none-eabi-9-2019-q4-major-src.tar.bz2** gcc sources used to build toolchain.
* **build-toolchain.patch** gcc patch that has to applied to build SAT toolchain.
* **pkg** directory with built SAT toolchain for Windows and Linux.

